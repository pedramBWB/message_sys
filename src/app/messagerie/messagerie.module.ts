import { NbActionsModule, NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BottombarComponent } from './bottombar/bottombar.component';
import { InboxComponent } from './inbox/inbox.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SendInputComponent } from './send-input/send-input.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    BottombarComponent,
    InboxComponent,
    NavbarComponent,
    SidebarComponent,
    SendInputComponent,
  ],
  imports: [
    CommonModule,
    NbLayoutModule,
    NbSidebarModule,
    NbCardModule,
    NbMenuModule,
    NbAlertModule,
    NbInputModule,
    NbActionsModule,
    NbButtonModule,
    FormsModule,
  ],
  exports: [
    BottombarComponent,
    InboxComponent,
    NavbarComponent,
    SidebarComponent,
    SendInputComponent,
  ]
})
export class MessagerieModule { }

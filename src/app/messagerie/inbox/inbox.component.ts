import { Message } from '../send-input/message';
import { MessageDataService } from '../services/message-data.service';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from '@angular/core';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  messages: Message[] = []

  badge: number;

  constructor(private service: MessageDataService) {
    this.badge = 0;
   }

  @Input() content = "contenu du component"
  @Output() dataevent = new EventEmitter()

  ngOnInit(): void {

    this.messages = this.service.messages

    // this.service.messagesList().subscribe((datas) => {
    //   this.messages = datas
    // })

    this.badge = this.messages.length

  }

  notifs() {
    this.badge = this.messages.length
    // console.log(this.messages.length);
  }

  // deleteMessage(id: number){
  //   this.service.deleteMessageById(id)
  //   console.log(id)
  //   this.badge = this.messages.length

  //   this.service.messagesList().subscribe((datas) => {
  //     this.messages = datas
  //   })
  // }

  ngOnChanges(changes: SimpleChange) {
    console.log(changes);
  }

}

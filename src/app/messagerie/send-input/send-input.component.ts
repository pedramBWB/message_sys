import { MessageDataService } from '../services/message-data.service';
import { Message } from './message';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-send-input',
  templateUrl: './send-input.component.html',
  styleUrls: ['./send-input.component.css']
})
export class SendInputComponent implements OnInit {

    // Default Message
    m: Message = {
      username : '',
      title : '',
      content : '',
      isRead : false,
      sent : new Date()
    };

  constructor(
    private service : MessageDataService
  ) {
    // this.m = {
    //   username: '',
    //   title: '',
    //   content: '',
    //   isRead: false,
    //   sent: new Date()
    // }
   }

  ngOnInit(): void {
    // this.m = {
    //   username : '',
    //   title: '',
    //   content : '',
    //   isRead: false,
    //   sent: new Date()
    // }
  }

  addNewMessage() {
    this.service.addMessage(this.m);

    console.log("it's here");
    // this.resetMessage()
  }

  // resetMessage(){
  //   this.m = {
  //     username : '',
  //     title: '',
  //     content : '',
  //     isRead: false,
  //     sent: new Date()
  //   }
  // }


}

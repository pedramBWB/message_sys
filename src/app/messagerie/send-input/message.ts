export interface Message {

  username: string;
  title: string;
  content: string;
  isRead: boolean;
  sent: Date;

}

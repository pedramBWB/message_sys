import { Message } from '../send-input/message';
import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessageDataService {


  private subject = new BehaviorSubject<Message[]>([]);
  messages: Message[] = [];

  constructor(
    // private http: HttpClient
  ) {
    // this.getMessagesFromApi();
    // this.messages = ''
  }

  // getMessagesFromApi(){

  //   this.http.get<any>('http://localhost:3333/messages')
  //     .pipe()
  //     .subscribe(
  //       (response) => {
  //         this.subject.next(response)
  //       },
  //       (error) => {
  //         console.log(error);
  //       },
  //       () => {
  //         console.log("All messages loaded");
  //       }
  //     )
  // }

  messagesList(){
    return this.subject.asObservable();
  }

  getMessages(): Message[]{
    return this.messages;
  }

  addMessage(messageToAdd: Message){

    this.messages.push(messageToAdd)

    // const msgs = this.subject.getValue();
    // msgs.push(messageToAdd);
    // this.subject.next(msgs);

    // this.http.get('http://localhost:3333/messages')
    //   .pipe()
    //   .subscribe(
    //     (response) => {
    //       console.log(response);
    //     },
    //     (error) => {
    //       console.log(error);
    //     },
    //     () => {
    //       console.log("Message added succesfully");
    //     }
    //   )
  }

  // getObeservableMessage(): Observable<Message[]>{
  //   return this.subject.asObservable()
  // }

  // getSubjectMessages(): Observable<Message[]> {
  //   return this.subject.asObservable();
  // }

  // deleteMessageById(id: number){
  //   this.http.delete<any>(`http://localhost:3333/messages/${id}`)
  //   .subscribe(
  //     (response) => {
  //       console.log(response);
  //     },
  //     (error) => {
  //       console.log(error)
  //     },
  //     () => {
  //       console.log("Selected message has been deleted");
  //     }
  //   )
  // }

  getUnreadMessageAsObservable(): Observable<Message[]>{
    return this.subject.pipe(
      map(msgs => msgs.filter(m => !m.isRead))
    )
  }

  getCountOfUnreadMessages(): Observable<number>{
    return this.subject.pipe(
      map(msgs => msgs.filter(m => !m.isRead)),
      map(unreadMessages => unreadMessages.length)
    );
  }
}

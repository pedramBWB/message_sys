import { SendInputComponent } from './messagerie/send-input/send-input.component';
import { InboxComponent } from './messagerie/inbox/inbox.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'inbox', component:InboxComponent},
  {path: 'sendInput', component:SendInputComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
